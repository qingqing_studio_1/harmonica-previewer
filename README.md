# HarmonicaPreviewer

#### 介绍
配套HarmonyOS北向IDE DevEco使用的HAP预览器，该预览器具备如下技术特征：

1、基于HarmonyOS引擎进行JS bundle的加载和渲染

2、基于WebSocket与IDE进行通信

3、提供基于浏览器/WebView的图形图像展现和交互

基于以上技术特征，该预览器可以做到对HarmonyOS北向开发的最大程度预览，可以确保真实的界面效果展示，可以让北向开发者在没有实体硬件的情况下进行应用开发和适配，且不需要利用云端进行调试。

#### 软件架构
软件架构图如下：


![输入图片说明](https://images.gitee.com/uploads/images/2020/0923/120424_297221c8_5557538.png "屏幕截图.png")


#### 安装教程

待补充

#### 使用说明

待补充

#### 参与贡献

1.  Fork 本仓库
2.  提交代码
3.  新建 Pull Request


#### 联系方式

leo@hiharmonica.com

