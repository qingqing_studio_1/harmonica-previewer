# HarmonicaPreviewer

#### Description

HarmonicaPreviewer is a tool for HarmonyOS IDE DevEco, it is built on following technologies:
1. Rendering engine from HarmonyOS libraries.
2. WebSocket for communication with IDE.
3. Provides browser/WebView based graphic display and UI.
Thus, it's able to preview HarmonyOS apps in a high-fidelity way, it helps the app developer to launch and test their apps before they really get the hardware, also they don't need the cloud-based simulators.

#### Software Architecture
Software architecture description

(https://images.gitee.com/uploads/images/2020/0923/121250_3d6ba368_5557538.png "屏幕截图.png")

#### Installation

TBF

#### Instructions

TBF

#### Contribution

1.  Fork the repository
2.  Commit your code
3.  Create Pull Request


#### Contact

leo@hiharmonica.com